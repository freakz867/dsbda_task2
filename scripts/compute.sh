#!/bin/bash

while true; do
  sleep 5
  java -Xmx2048m -Xms128m -jar "$1" "${PERIOD:-5}"
done
