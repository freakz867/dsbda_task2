#!/bin/bash

source twitter.conf

function url-encode() {
  perl -MURI::Escape -e 'print uri_escape($ARGV[0]);' "$1"
}

function build-signature-parameters() {
  url-encode `printf '%s\n' "$@" |
  sort |
  xargs -d '\n' printf '%s&' |
  head -c -1`
}

# Signs and executes HTTP-request via curl
function twitter-request() {
  timestamp="`date +%s`"

  nonce=`head -c 32 /dev/urandom |
  base64 |
  xargs -d '\n' printf '%s' |
  sed 's/[^A-Za-z0-9]//g'`

  method="$1"
  url="$2"
  shift 2

  sign_parameters=`build-signature-parameters "$@" \
  'oauth_version=1.0' \
  "oauth_timestamp=$timestamp" \
  "oauth_nonce=$nonce" \
  'oauth_signature_method=HMAC-SHA1' \
  "oauth_consumer_key=$consumer_key" \
  "oauth_token=$access_token"`

  value="$method&`url-encode "$url"`&$sign_parameters"
  key="$consumer_secret&$access_token_secret"

  signature=`echo -n "$value" |
  openssl dgst -sha1 -hmac "$key" -binary |
  base64`

  oauth_args=`printf '%s, ' \
  'oauth_version="1.0"' \
  "oauth_timestamp=\"$timestamp\"" \
  "oauth_nonce=\"$nonce\"" \
  'oauth_signature_method="HMAC-SHA1"' \
  "oauth_consumer_key=\"$consumer_key\"" \
  "oauth_token=\"$access_token\"" \
  "oauth_signature=\"\`url-encode "$signature"\`\"" |
  head -c -2`

  parameters=()
  for parameter in "$@"; do
    parameters+=(-d "$parameter")
  done

  curl -s -H "Authorization: OAuth $oauth_args" -X "$method" "$url" "${parameters[@]}"
}

function handle_events() {
  grep -Po --line-buffered '(?<="timestamp_ms":")\d+(?=")'
}

url='https://stream.twitter.com/1.1/statuses/filter.json'
filter=('track=bitcoin' 'track=ethereum')

while true; do
  twitter-request 'POST' "$url" "${filter[@]}" | handle_events
  sleep 1
done
