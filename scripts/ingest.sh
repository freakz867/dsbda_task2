#!/bin/bash

while ! cqlsh cassandra -e 'select * from task.event;' > /dev/null; do
  cqlsh cassandra -f /scripts/task.cql
  sleep 1
done

echo 'Ingest started'
cd /scripts || exit 1

# Debounces input by given time or maximum count of values
function timed-buffer() {
  time="$1"
  batch="$2"

  while true; do
    start=`date +%s`
    buffer=''
    count=0

    while (( $((`date +%s` - $start)) < $time )); do
      read -rd $'\n' line
      buffer="$buffer $line"
      count=$(($count + 1))
      (($count >= $batch)) && break
    done

    [ -n "$buffer" ] && echo "$buffer"
  done
}

./stream.sh | timed-buffer 5 40 | while read -rd $'\n' timestamps; do
  timestamps=($timestamps)
  inserts=""

  for timestamp in "${timestamps[@]}"; do
    inserts="$inserts insert into event(id, time) values (`uuidgen`, $timestamp);"
  done

  cqlsh cassandra -e "use task; begin batch$inserts apply batch;"
done
