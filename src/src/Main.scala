import java.net.InetSocketAddress
import java.text.SimpleDateFormat
import java.util.Date

import scala.collection.mutable._

import com.datastax.driver.core._

import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming._
import org.apache.spark.streaming.dstream.DStream

/**
 * Main application object.
 * Calculates count timestamps per period.
 */
object Main extends App {
  val period = args.headOption.map(_.toInt).getOrElse(5)

  val cassandraHost = "cassandra"
  val cassandraPort = 9042

  /**
   * Query timestamps from Cassandra database.
   */
  def queryEvents(): List[Long] = {
    // Connect to Cassandra
    val session = Cluster.builder
      .addContactPointsWithPorts(new InetSocketAddress(cassandraHost, cassandraPort))
      .build.connect("task")

    // Fetch events
    val result = session.execute("select * from event")
    val events = Iterator.continually(result.one()).takeWhile(_ != null)
      .map(_.get("time", classOf[Date]).getTime).toList

    session.close()
    events
  }

  /**
   * Calculate events per period.
   *
   * @param context Spark streaming context
   * @param events List of timestamps
   * @param period Calculation period
   */
  def calculate(context: StreamingContext, events: List[Long], period: Int): DStream[(Long, Int)] = {
    // Group events by key and parallelize
    val rdds = events.groupBy(_ / 1000 / period).values
      .map(group => context.sparkContext.parallelize(Seq(group)))

    val queue = rdds.foldLeft(new Queue[RDD[List[Long]]])(_ += _)
    context.queueStream(queue).map(x => (x.head / 1000 / period * period, x.length))
  }

  /**
   * Calculates events ones and stops streaming context.
   *
   * @param dstream Spark stream
   */
  def collectOnce(dstream: DStream[(Long, Int)]): Seq[(Long, Int)] = {
    val buffer = new ListBuffer[(Long, Int)]

    dstream.foreachRDD { rdd =>
      val data = rdd.collect
      buffer ++= data
      if (data.isEmpty) {
        dstream.context.stop();
      }
    }

    dstream.context.start()
    dstream.context.awaitTermination()
    buffer
  }

  val sparkConf = new SparkConf()
    .setAppName("SparkCalculator")
    .setMaster("local[*]")
    .set("spark.shuffle.service.enabled", "true")
    .set("spark.dynamicAllocation.enabled", "true")
  val context = new StreamingContext(sparkConf, Seconds(1))
  context.sparkContext.setLogLevel("ERROR")

  val events = queryEvents()
  val dstream = calculate(context, events, period)
  val result = collectOnce(dstream)

  println("Result:")
  result.sortBy(_._1).foreach {
    case (time, count) =>
      println(s"time: $time, events: $count")
  }

  sys.exit(0)
}
