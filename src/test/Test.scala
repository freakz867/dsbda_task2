import org.apache.spark.SparkConf
import org.apache.spark.streaming._

import org.scalatest.FlatSpec

import scala.collection.mutable.ListBuffer

class Test extends FlatSpec {
  "Main" should "calculate events once using Spark Streaming" in {
    val conf = new SparkConf()
      .setAppName("SparkCalculator")
      .setMaster("local[*]")
    val context = new StreamingContext(conf, Seconds(1))
    context.sparkContext.setLogLevel("ERROR")

    val events = List(1, 3, 7, 8, 9, 12).map(_ * 1000L)
    val dstream = Main.calculate(context, events, 5)
    val result = Main.collectOnce(dstream)

    assert(result.toSet == Set((0, 2), (5, 3), (10, 1)))
  }
}
